//
//  NextController.swift
//  COMP3912-Week03-NavigationTestApp
//
//  Created by Massimo Savino on 2016-09-26.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import UIKit
import AVFoundation
import Speech
import QuartzCore

private enum AccentCountries: Int {
    
    case AccentAU
    case AccentIE
    case AccentZA
    case AccentUK
    case AccentUS
    
    case AccentAustralia
    case AccentIreland
    case AccentSouthAfrica
    case AccentUnitedKingdom
    case AccentUnitedStates
    
    var stringRepresentation: String {
        switch self {
        case .AccentAU:
            return Constants.NextController.AccentAU
        case .AccentIE:
            return Constants.NextController.AccentIE
        case .AccentZA:
            return Constants.NextController.AccentZA
        case .AccentUK:
            return Constants.NextController.AccentUK
        case .AccentUS:
            return Constants.NextController.AccentUS
        case .AccentAustralia:
            return Constants.NextController.AccentAustralia
        case .AccentIreland:
            return Constants.NextController.AccentIreland
        case .AccentSouthAfrica:
            return Constants.NextController.AccentSouthAfrica
        case .AccentUnitedKingdom:
            return Constants.NextController.AccentUnitedKingdom
        case .AccentUnitedStates:
            return Constants.NextController.AccentUnitedStates
//        default:
//            return Constants.NextController.AccentMissing
        }
    }
}


class NextController: UIViewController {
    
    // MARK: Properties
    
    public var audibleText: String? = ""
    
    @IBOutlet weak var audibleTextLabel: UILabel!
    @IBOutlet weak var pitchLabel: UILabel!
    @IBOutlet weak var pitchSlider: UISlider!
    
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var rateSlider: UISlider!
    
    @IBOutlet weak var accents: UISegmentedControl!
    @IBOutlet weak var speakButton: UIButton!
    
    var chosenAccent: String? = Constants.NextController.AccentUK


    
    
    // MARK: Lifecycle

    override func viewDidLoad() {
        
        super.viewDidLoad()
        setUp()
    }
    
    
    
    func setUp() {

        audibleTextLabel.text = audibleText
        audibleTextLabel.numberOfLines = 4
        audibleTextLabel.lineBreakMode = .byTruncatingTail
        
        pitchLabel.text = Constants.NextController.PitchLabel
        rateLabel.text = Constants.NextController.RateLabel
        
        for individualAccent in 0..<accents.numberOfSegments {
            accents.setTitle(AccentCountries(rawValue: individualAccent)?.stringRepresentation, forSegmentAt: individualAccent)
        }
        
        speakButton.setTitle(Constants.NextController.SpeakButton, for: .normal)

        accents.addTarget(self, action: #selector(NextController.indexChanged(_:)), for: .valueChanged)
        self.view.addSubview(accents)
        
        
    }

    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        
        switch accents.selectedSegmentIndex {
        case 0:
            chosenAccent = Constants.NextController.AccentAU
        case 1:
            chosenAccent = Constants.NextController.AccentIE
        case 2:
            chosenAccent =  Constants.NextController.AccentZA
        case 3:
            chosenAccent =  Constants.NextController.AccentUK
        case 4:
            chosenAccent =  Constants.NextController.AccentUS
        default:
            chosenAccent = Constants.NextController.AccentUK
        }
    }
    
    
    @IBAction func speakYourBrains(_ sender: AnyObject) {
        
        let moog = AVSpeechSynthesizer()
        let moogTalk = AVSpeechUtterance(string: audibleText!)
        
        moogTalk.rate = rateSlider.value
        moogTalk.pitchMultiplier = pitchSlider.value

        moogTalk.voice = AVSpeechSynthesisVoice(language: chosenAccent)
        
        moog.speak(moogTalk)
        print(moogTalk.voice)
    }
}
