//
//  Constants.swift
//  COMP3912-Week03-NavigationTestApp
//
//  Created by Massimo Savino on 2016-09-26.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import Foundation


struct Constants {
    
    struct Segues {
        
        static let ShowNextController = "ShowNextController"
    }
    
    struct ViewController {
        
        static let TextLabel = "Please enter the text you'd like this app to say on the next screen"
        static let SpeakButton = "PREPARE TO SPEAK YOUR BRAINS\n(on the next screen)"
        
        static let AlertTitle = "Can't enter blank text"
        static let AlertMessage = "Press OK to re-enter text"
        static let OK = "OK"
    }
    
    struct NextController {
        
        static let PitchLabel = "Slide to change the audible text's pitch"
        static let RateLabel = "Slide to change how fast the audible text is spoken"
        static let TitleLabel = "Next Controller"
        static let SpeakButton = "SPEAK YOUR BRAINS"
        
        static let AccentAustralia = "en-AU"
        static let AccentAU = "AU"
        static let AccentIreland = "en-IE"
        static let AccentIE = "IE"
        static let AccentSouthAfrica = "en-ZA"
        static let AccentZA = "ZA"
        static let AccentUnitedKingdom = "en-GB"
        static let AccentUK = "UK"
        static let AccentUnitedStates = "en-US"
        static let AccentUS = "US"
    }
}
