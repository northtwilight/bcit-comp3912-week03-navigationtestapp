//
//  ViewController.swift
//  COMP3912-Week03-NavigationTestApp
//
//  Created by Massimo Savino on 2016-09-26.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import UIKit
import AVFoundation
import Speech
import QuartzCore

class ViewController: UIViewController {
    
    // MARK: Properties
    
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var textView: UITextView!

    @IBOutlet weak var speakButton: UIButton!
    
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setUp()
    }
 
    
    
    // MARK: Set up UI 
    
    func setUp() {
        
        textLabel.text = Constants.ViewController.TextLabel
        
        speakButton.titleLabel?.lineBreakMode = .byWordWrapping
        speakButton.setTitle(Constants.ViewController.SpeakButton, for: .normal)
        
        
        textView.layer.borderWidth = 0.5
        textView.layer.borderColor = UIColor.black.cgColor
        textView.layer.cornerRadius = 5.0
    }
    
    
    
    // MARK: Storyboard segue to next controller
    
    @IBAction func moveToNextController(_ sender: AnyObject) {
        
        guard let enteredText = textView.text, !enteredText.isEmpty else {
            
            let alertController = UIAlertController(title: Constants.ViewController.AlertTitle, message: Constants.ViewController.AlertMessage, preferredStyle: .alert)
            let defaultAlertAction = UIAlertAction(title: Constants.ViewController.OK, style: .default)
            
            alertController.addAction(defaultAlertAction)
            present(alertController, animated: true, completion: nil)
            
            return
        }
        print(enteredText.characters.count)
        
        self.performSegue(withIdentifier: Constants.Segues.ShowNextController, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == Constants.Segues.ShowNextController {
            
            if let destinationViewController = segue.destination as? NextController {

                destinationViewController.audibleText = textView?.text
            }
        }
    }
}

